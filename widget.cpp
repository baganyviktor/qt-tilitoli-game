#include "widget.h"
#include <time.h>

/*  Készítsünk programot, amellyel a következő kétszemélyes játékot lehet játszani.
    Adott egy n × n mezőből álló tábla, ahol a két játékos bábúi egymással szemben
    helyezkednek el, két sorban (pont, mint egy sakktáblán, így mindkét játékos 2n
    bábuval rendelkezik, ám mindegyik bábu ugyanolyan típusú). A játékos bábúival
    csak előre léphet egyenesen, vagy átlósan egy mezőt (azaz oldalra, és hátra felé
    nem léphet), és hasonlóan ütheti a másik játékos bábúját előre átlósan
    (egyenesen nem támadhat). Az a játékos győz, aki először átér a játéktábla másik
    végére egy bábuval.
    A program biztosítson lehetőséget új játék kezdésére a táblaméret megadásával
    (6 × 6, 8 × 8, 10 × 10), és ismerje fel, ha vége a játéknak. Ekkor jelenítse meg,
    melyik játékos győzött, majd automatikusan kezdjen új játékot. */

beadando::beadando(QWidget *parent) : QWidget(parent)
{
    setMinimumSize(600, 100);
    setBaseSize(600,600);
    setWindowTitle(trUtf8("Tili-Toli"));

    newGameButton = new QPushButton();
    newGameButton->setText(trUtf8("Új játék kezdése"));
    connect(newGameButton, SIGNAL(clicked()), this, SLOT(NewGameButtonClicked()));

    /*
     * A legördülő menü megkonstruálása
    */
    palyameret = new QComboBox();
    palyameret->addItem("6x6");
    palyameret->addItem("8x8");
    palyameret->addItem("10x10");

    buttonContainer = new QGridLayout();

    mainView = new QVBoxLayout();
    mainView->addWidget(palyameret);
    mainView->addWidget(newGameButton);

    gamedata = new int*[10];
    buttonData.resize(10);
    for(int i=0; i<10;i++)
    {
        gamedata[i] = new int[10];
        buttonData[i].resize(10);
        for(int j=0; j<10;j++)
        {
            gamedata[i][j] = 0;
            buttonData[i][j] = new QPushButton();
            buttonData[i][j]->setFont(QFont("Times New Roman", 30, QFont::Bold));
            buttonData[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            buttonData[i][j]->hide();
            buttonContainer->addWidget(buttonData[i][j],i,j);
            connect(buttonData[i][j],SIGNAL(clicked(bool)),this,SLOT(ButtonClicked()));
        }
    }
    mainView->addLayout(buttonContainer);

    setLayout(mainView);
}

void beadando::StartGame()
{
    setMinimumSize(600,600);
    palyameret->hide();
    lepesekszama = 0;

    for(int i=0; i<getMeret(); i++)
    {
        for(int j=0; j<getMeret(); j++)
        {
            buttonData[i][j]->show();
            buttonData[i][j]->setEnabled(true);
        }
    }
    ResetView();
    GenerateRandomTable();
    UpdateGameView();
}

void beadando::GenerateRandomTable()
{
    srand (time(NULL));
    int randx = 0; int randy = 0;
    for(int i=1; i<=babukszama; i++)
    {
        do
        {
            randx = rand()%getMeret();
            randy = rand()%getMeret();
        } while(gamedata[randx][randy] != 0);
        gamedata[randx][randy] = i;
    }
}

void beadando::StopGame()
{
    HideButtons();
    palyameret->show();
}

int beadando::getMeret()
{
    switch (palyameret->currentIndex()) {
    case 0:
        return 6;
        break;
    case 1:
        return 8;
    case 2:
        return 10;
    default:
        return 6;
        break;
    }
}

void beadando::StepGame(const int x, const int y)
{
    if(x-1 >= 0 && gamedata[x-1][y] == 0)
    {
        BabuMozgat(x,y,x-1,y);
    } else if(x+1 < getMeret() && gamedata[x+1][y] == 0)
    {
        BabuMozgat(x,y,x+1,y);
    } else if(y-1 >= 0 && gamedata[x][y-1] == 0)
    {
        BabuMozgat(x,y,x,y-1);
    } else if(y+1 < getMeret() && gamedata[x][y+1] == 0)
    {
        BabuMozgat(x,y,x,y+1);
    }
    lepesekszama++;

    CheckWin();
}

void beadando::CheckWin()
{
    if(false)
    {
        msgBox.setText(jatekuzenet);
        msgBox.exec();
    }
}

void beadando::BabuMozgat(const unsigned int x, const unsigned int y, const unsigned int newx, const unsigned int newy)
{
    gamedata[newx][newy] = gamedata[x][y];
    gamedata[x][y] = 0;
}

void beadando::NewGameButtonClicked()
{
    babukszama = (getMeret()*getMeret())-1;
    StartGame();
}

void beadando::ButtonClicked()
{
    QPushButton* senderButton = dynamic_cast <QPushButton*> (QObject::sender());
    int location = buttonContainer->indexOf(senderButton);
    int x = location / 10;
    int y = location % 10;

    StepGame(x,y);
    UpdateGameView();
}
void beadando::UpdateGameView()
{
    for(int i=0; i<10; ++i)
    {
        for(int j=0; j<10; ++j)
        {
            if(gamedata[i][j] == 0)
            {
                buttonData[i][j]->setText(trUtf8(""));
                buttonData[i][j]->setEnabled(false);
            }
            else
            {
                buttonData[i][j]->setText(QString::number(gamedata[i][j]));
                buttonData[i][j]->setEnabled(true);
            }
        }
    }
}
void beadando::ResetView()
{
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            gamedata[i][j] = 0;
        }
    }
    UpdateGameView();
}
void beadando::HideButtons()
{
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            buttonData[i][j]->setEnabled(false);
            buttonData[i][j]->hide();
        }
    }
}

