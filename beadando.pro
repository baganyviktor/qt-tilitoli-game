#-------------------------------------------------
#
# Project created by QtCreator
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = beadando
TEMPLATE = app

SOURCES += main.cpp \
    widget.cpp

HEADERS  += \
    widget.h

FORMS    +=
