#ifndef beadando_H
#define beadando_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <QPixmap>
#include <QIcon>
#include <QMessageBox>
#include <QString>

class beadando : public QWidget
{
    Q_OBJECT

    public:
        beadando(QWidget* parent = 0);

    private slots:
        void ButtonClicked();
        void NewGameButtonClicked();

    private:
        void NewGame();
        void StartGame();
        void StopGame();
        void SetupGame();
        void StepGame(const int x, const int y);
        void BabuMozgat(const unsigned int x, const unsigned int y, const unsigned int newx, const unsigned int newy);
        void UpdateGameView();
        void ResetView();
        int getMeret();
        void GenerateRandomTable();
        void HideButtons();
        void CheckWin();

        int lepesekszama;
        int babukszama;
        int** gamedata;
        QString jatekuzenet;
        QGridLayout* buttonContainer;
        QVBoxLayout* mainView;
        QComboBox* palyameret;
        QPushButton* newGameButton; // új játék gombja
        QVector<QVector<QPushButton*>> buttonData; // gombtábla
        QMessageBox msgBox;
};

#endif // QUEEN_H
